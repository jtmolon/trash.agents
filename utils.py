# -*- coding:utf-8 -*-

def check_finish(trashes):
    """Verifica se todos os lixos foram esvaziados
    """
    trashes0 = filter(lambda trash: trash.kind == 0, trashes)
    trashes1 = filter(lambda trash: trash.kind == 1, trashes)
    finish0 = True
    finish1 = True
    for trash in trashes0:
        if trash.amount:
            finish0 = False
            break
    for trash in trashes1:
        if trash.amount:
            finish1 = False
            break

    return finish0, finish1

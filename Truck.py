# -*- coding: utf-8 -*-
from config import *
import pygame
import math
import random

class Truck(pygame.sprite.Sprite):
    def __init__(self, cor, (x, y, width, height), kind=0):
        pygame.sprite.Sprite.__init__(self)           
        self.imageMaster = pygame.Surface((width,height))
        self.imageMaster.fill(cor)
        self.imageMaster = self.imageMaster.convert()
        self.image = self.imageMaster
        #get the rectangular area of the Surface
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        #angulo
        self.degree = 180
        #x/y temporario
        self.dx = 0
        self.dy = 0
        #velocidade de movimentacao
        self.speed = SPEED
        if not self.speed:
            self.speed = random.uniform(0.1,1)
        self.target = None
        self.colidindo = False
        self.kind = kind
        self.amount = 0.0
        self.max_amount = 3.0
        self.ways = []
        self.way = {"way": [self.degree,], "reward": 0, "trashes": 0, "mean": 0}
        self.back_way = [self.degree,]
        self.finish = False

    def update(self, streets, trashes):
        """Atualiza a posicao da imagem no screen
        """
        self.calcPos()
        collide = pygame.sprite.spritecollide(self, streets, False)
        collide_trash = pygame.sprite.spritecollide(self, trashes, False)

        if collide_trash and not self.amount >= self.max_amount and not self.finish:
            trash = collide_trash[0]
            if trash.kind == self.kind:
                #print "Esvaziou dio"
                self.way["trashes"] += 1
                self.way["reward"] += trash.amount
                self.amount += trash.amount
                #print self.amount
                trash.image.set_alpha(70)
                trash.amount = 0

                #se o caminhao encheu
                if self.amount >= self.max_amount:
                    self.way["mean"] = self.way["reward"] / self.way["trashes"]
                    self.ways.append(self.way)
                    self.way = {"way": [self.degree,], "reward": 0, "trashes": 0, "mean": 0}
                    self.degree = self.back_way.pop()


        #caso colida, retorna a posicao anterior
        if collide:
            if len(collide) == 1:
                self.x += self.dx
                self.y += self.dy
                self.rect.centerx = self.x
                self.rect.centery = self.y
                self.colidindo = False
            else:
                if not self.colidindo:
                    olddegree = self.degree
                    #se o caminhao esta cheio, pegar do self.back_way
                    if self.amount >= self.max_amount or self.finish:
                        self.degree = self.back_way.pop()
                        if not self.back_way:
                            self.amount = 0.0
                            if self.finish:
                                self.speed = 0
                    else:
                        self.test_collide(streets) 
                    self.colidindo = True
                    if olddegree == 0:
                        self.calcPos(11,0)
                    elif olddegree == 90:
                        self.calcPos(0,-11)
                    elif olddegree == 180:
                        self.calcPos(-11,0)
                    else:
                        self.calcPos(0,11)
                else:
                    self.calcPos()
                self.x += self.dx
                self.y += self.dy
                self.rect.centerx = self.x
                self.rect.centery = self.y

    def test_collide(self, streets):
        possibilidades = []
        oldX = self.x
        oldY = self.y
        oldRectcenterx = self.rect.centerx
        oldRectcentery = self.rect.centery
        contrarios = {}
        contrarios[0] = 180
        contrarios[90] = 270
        contrarios[180] = 0
        contrarios[270] = 90
        default_degree = [0,90,180,270]
        default_degree.remove(contrarios.get(self.degree))

        for i in default_degree:
            if i == 0:
                self.calcPos(26,0)
            elif i == 90:
                self.calcPos(0,-26)
            elif i == 180:
                self.calcPos(-26,0)
            else:
                self.calcPos(0,26)
            
            self.x += self.dx
            self.y += self.dy
            self.rect.centerx = self.x
            self.rect.centery = self.y

            collide = pygame.sprite.spritecollide(self, streets, False)
            if collide:
                possibilidades.append(i)

            self.x = oldX
            self.y = oldY
            self.rect.centerx = oldRectcenterx
            self.rect.centery = oldRectcentery

        if possibilidades:
            escolha = random.choice(possibilidades)
            self.degree = escolha
            self.way["way"].append(self.degree)
            self.back_way.append(contrarios.get(self.degree))

        return True

    def turnLeft(self):
        """Rotaciona a imagem para esquerda
        """
        self.degree += 1
        if self.degree >= 360:
            self.degree = 0

    def turnRight(self):
        """Rotaciona a imagem para direita
        """
        self.degree -= 1
        if self.degree < 0:
            self.degree = 355

    def turnUp(self):
        """ Velocidade positiva - anda para frente
        """
        self.speed = 1

    def turnDown(self):
        """ Velocidade negativa - anda para tras
        """
        self.speed = -1

    def calcPos(self, plusx_speed=0, plusy_speed=0):
        """  
            Testa o angulo da imagem e atribui valores para Dx/Dy.
            Multiplica Dx/Dy com speed, isso faz a imagem se mover no mesmo sentido atual.
        """        
        if self.degree == 0:
            self.dx = 1
            self.dy = 0 
        elif self.degree == 90: 
            self.dx = 0 
            self.dy = -1
        elif self.degree == 180:
            self.dx = -1
            self.dy = 0 
        else:#270
            self.dx = 0 
            self.dy = 1
        self.dx *= self.speed
        self.dy *= self.speed
        self.dx += plusx_speed
        self.dy += plusy_speed

    def dropTarget(self, arena):
        """ Larga o target, caso o robot esteja levando um
        """
        if self.target:
            #se estamos na arena
            if pygame.sprite.spritecollideany(self.target, arena):
                if self.target.x > 290:
                    incx = -40
                else:
                    incx = 40
                if self.target.y > 160:
                    incy = -40
                else:
                    incy = 40
            else:
                if ((self.target.x > 0 and self.target.x < 100 - 40) or (self.target.x > 700 and self.target.x < 800 - 40)) and (self.target.y > 100 - 40 and self.target.y < 440):                    
                    incx = 0
                elif self.target.x > 400:
                    incx = -40
                else:
                    incx = 40                    
                
                if (self.target.y > 0 and self.target.y < 100) and (self.target.x > 100 - 40 and self.target.x < 700):
                    incy = 0
                elif self.target.y > 540:
                    incy = -40
                else:
                    incy = 40

            self.target.x += incx
            self.target.y += incy
            self.target.rect.centerx += incx
            self.target.rect.centery += incy
            self.target = None

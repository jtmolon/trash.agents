# -*- coding: utf-8 -*-

import pygame

class Arena(pygame.sprite.Sprite):
    
    def __init__(self, cor, (x, y, width, height)):
        pygame.sprite.Sprite.__init__(self)
        self.cor = cor
        self.image = pygame.Surface((width, height))
        self.image.fill(cor)
        self.image.set_colorkey((0,0,0))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


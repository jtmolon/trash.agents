# -*- coding: utf-8 -*-
from Robot import *
from Truck import *
from Street import Street
from Block import Block
from Target import *
from Arena import *
from Trash import *
from utils import check_finish
from config import *
import pygame
import random
pygame.init()

size = (25,25)
white = (255, 255, 255)
blue = (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 50)
darkgreen = (35,142,35)
black = (0, 0, 0)
gray = (168, 168, 168)
brown = (92, 51, 23)
coral = (255, 127, 0)
yellow = (255, 255, 0)

list_blocks = []
for i in range(0,5):
    for j in range(0,5):
        x = j * 104 + j * 30 + 30
        y = i * 104 + i * 30 + 30
        list_blocks.append((x, y, 104, 104))

list_streets = []
for i in range(0,6):
    x = i * 104 + i * 30 + 10
    list_streets.append((x, 10, 10, 680))
    
for i in range(0,6):
    y = i * 104 + i * 30 + 10
    list_streets.append((10, y, 680, 10))


def main():
    screen = pygame.display.set_mode((900, 700))
    pygame.display.set_caption("Combat Biplane")
    pygame.key.set_repeat(1, 1)

    background = pygame.Surface(screen.get_size())
    background.fill(black)

    truck = Truck(white,(696,15,13,13))
    truckgroup = pygame.sprite.Group(truck)

    truck = Truck(white,(696,15,13,13))
    truckgroup.add(truck)

    truck = Truck(coral,(696,15,13,13),1)
    truckgroup.add(truck)

    truck = Truck(coral,(696,15,13,13),1)
    truckgroup.add(truck)

    #criar as quadras
    blocks = pygame.sprite.Group()
    for block in list_blocks:
        blocks.add(Block(gray,block))
    #criar as estradas
    streets = pygame.sprite.Group()
    for street in list_streets:
        streets.add(Street(blue,street))
    #targets = pygame.sprite.Group()
    #for x in range(0,5):
    #    x = random.randrange(111,570)
    #    y = random.randrange(111,310)
    #    targets.add(Target(red,x,y,(8,8),screen))
    #criar as lixeiras
    trashs = pygame.sprite.Group()
    for block in blocks.sprites()[:LIMIT_TRASHES]:
        if random.randrange(0, 2):
            #cria lixeira da E->D e C->B
            topleft = block.rect.topleft
            if random.randrange(0, 2):
                x = topleft[0] - 10 + random.randrange(0, 95)
                y = topleft[1] - 10
                xx = x + 10
                yy = y 
            else:
                x = topleft[0] - 10
                y = topleft[1] - 10 + random.randrange(0, 95)
                xx = x 
                yy = y + 10
        else:
            #cria lixeira da D->E e B->C
            bottomright = block.rect.bottomright
            if random.randrange(0, 2):
                x = bottomright[0] - random.randrange(0, 95)
                y = bottomright[1]
                xx = x + 10
                yy = y
            else:
                x = bottomright[0] 
                y = bottomright[1] - random.randrange(0, 95)
                xx = x
                yy = y + 10
        trashs.add(Trash(green,(x,y,10,10)))
        trashs.add(Trash(yellow,(xx,yy,10,10),1))

    clock = pygame.time.Clock()
    keepGoing = True
    while keepGoing:
        # taxa do frame
        clock.tick(100)
        #robots
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    robot.turnLeft()
                elif event.key == pygame.K_RIGHT:
                    robot.turnRight()
                elif event.key == pygame.K_UP:
                    robot.turnUp()
                elif event.key == pygame.K_DOWN:
                    robot.turnDown()
                elif event.key == pygame.K_SPACE:
                    robot.dropTarget(arenagroup)

        #arenagroup.clear(screen, background)
        #robotgroup.clear(screen, background)
        blocks.clear(screen, background)
        streets.clear(screen, background)
        trashs.clear(screen, background)
        truckgroup.clear(screen, background)
        #targets.clear(screen, background)
        truckgroup.update(streets, trashs)
        #targets.update()

        finish0, finish1 = check_finish(trashs)
        for truck in truckgroup:
            if truck.kind:
                truck.finish = finish1
            else:
                truck.finish = finish0

        #desenha no screen
        #arenagroup.draw(screen)
        blocks.draw(screen)
        streets.draw(screen)
        trashs.draw(screen)
        truckgroup.draw(screen)
        #targets.draw(screen)
        pygame.display.flip()

main()

